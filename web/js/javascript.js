notUpdateData = false;

function homeOnload() {
	console.log('Starting loading products');
	var listHtml = '';
	for (var i = 0; i < PRODUCTS.length; i++) {
		if(PRODUCTS[i].offert) {
			console.log('Adding product ' + parseInt(i+1));
			listHtml += PRODUCTS[i].parseHtml();
		}
	};
	document.getElementById('offerts').innerHTML = listHtml;
	updateCart();
}

function shopOnload() {
	console.log('Start loading categories');
	var dropHtml = '<option value="-1">Tutti i prodotti</option>';
	for (var i = 0; i < CATEGORIES.length; i++) {
		dropHtml += '<option value="' + CATEGORIES[i].id + '">' + CATEGORIES[i].name + '</option>';
	};
	document.getElementById('filter').innerHTML = dropHtml;
	console.log('Starting loading products');
	var listHtml = '';
	for (var i = 0; i < PRODUCTS.length; i++) {
		console.log('Adding product ' + parseInt(i+1));
		listHtml += PRODUCTS[i].parseHtml();
	};
	document.getElementById('products').innerHTML = listHtml;
	updateCart();
}

function changeFilter() {
	console.log('Changing filter');
	var catId = document.getElementById('filter').value;
	var listHtml = '';
	for (var i = 0; i < PRODUCTS.length; i++) {
		if ((catId == -1) || (PRODUCTS[i].category == catId))
		{
			console.log('Adding product ' + parseInt(i+1));
			listHtml += PRODUCTS[i].parseHtml();
		}
	};
	document.getElementById('products').innerHTML = listHtml;
}

function userOnload() {
	var invoiceArray = getInvoiceData();
	if (invoiceArray != undefined) {
		console.log('Setting user invoice data');
		document.getElementById('invoice-name').value = invoiceArray[0];
		document.getElementById('invoice-last-name').value = invoiceArray[1];
		document.getElementById('invoice-country').value = invoiceArray[2];
		document.getElementById('invoice-province').value = invoiceArray[3];
		document.getElementById('invoice-city').value = invoiceArray[4];
		document.getElementById('invoice-cap').value = invoiceArray[5];
		document.getElementById('invoice-text').value = invoiceArray[6];
	}
	var shippingArray = getShippingData();
	if(shippingArray != undefined) {
		console.log('Setting user shipping data');
		document.getElementById('shipping-name').value = shippingArray[0];
		document.getElementById('shipping-last-name').value = shippingArray[1];
		document.getElementById('shipping-country').value = shippingArray[2];
		document.getElementById('shipping-province').value = shippingArray[3];
		document.getElementById('shipping-city').value = shippingArray[4];
		document.getElementById('shipping-cap').value = shippingArray[5];
		document.getElementById('shipping-text').value = shippingArray[6];
	}
	updateCart();
}

function cartOnload() {
	// Set cart review
	console.log('Loading cart review');
	var htmlText = '<li class="header"><div>Nome prodotto</div><div class="product-number">Qnt.</div><div class="product-price">Prezzo</div></li>';
	var totalPrice = 0;
	var totalNumber = 0;
	for (var i = 0; i < PRODUCTS.length; i++) {
		var cookie = readCookie('product-' + i);
		if (parseInt(cookie) > 0) {
			console.log('Loading product ' + i);
			productExist = true;
			var productPrice = PRODUCTS[i].price * parseInt(cookie);
			totalPrice += productPrice;
			totalNumber += parseInt(cookie);
			htmlText += '<li><div>' + PRODUCTS[i].name + '</div><div class="product-number">x' +
				cookie + '</div><div class="product-price">' +
				productPrice.toFixed(2) + ' €</div></li>';
		}
	}
	htmlText += '<li class="header"><div>TOTALE</div><div class="product-number">x' +
		totalNumber + '</div><div class="product-price">' + totalPrice.toFixed(2) + ' €</div></li>';

	document.getElementById('cart-review').innerHTML = htmlText;

	// Set user information review
	var invoiceCookie = getInvoiceData();
	var shippingCookie = getShippingData();
	var invoiceHtml = '';
	var shippingHtml = '';

	if (invoiceCookie == undefined) {
		invoiceHtml = '<a href="user.html?page=cart.html" alt="Pannello utente" title="Pannello utente"><button>Configura le informazioni per la fattura</button></a>';
	} else {
		invoiceHtml += invoiceCookie[0] + ' ' + invoiceCookie[1] + '<br>';
		invoiceHtml += invoiceCookie[2] + ' ' + invoiceCookie[4] + ' (' + invoiceCookie[3] + ') ' + invoiceCookie[5] + '<br>';
		invoiceHtml += invoiceCookie[6];
	}
	document.getElementById('invoice-data-review').innerHTML = invoiceHtml;

	if (shippingCookie == undefined) {
		shippingHtml = '<a href="user.html?page=cart.html" alt="Pannello utente" title="Pannello utente"><button>Configura le informazioni per la spedizione</button></a>';
	} else {
		shippingHtml += shippingCookie[0] + ' ' + shippingCookie[1] + '<br>';
		shippingHtml += shippingCookie[2] + ' ' + shippingCookie[4] + ' (' + shippingCookie[3] + ') ' + shippingCookie[5] + '<br>';
		shippingHtml += shippingCookie[6];
	}
	document.getElementById('shipping-data-review').innerHTML = shippingHtml;

	// Check if all conditions are positive, if yes, show the proceed button
	if (shippingCookie != undefined && invoiceCookie != undefined && totalNumber > 0) {
		console.log('Ready to create invoice');
		document.getElementById('pay').style.display = 'initial';
	}

	// Update cart on nav
	updateCart();
}

function updateUser() {
	if (!notUpdateData) {
		var isInvoiceValid = true;
		var isShippingValid = true;
		var resultText = '';
		console.log("Updating user information");
		var invoiceArray = [];
		invoiceArray.push(document.getElementById('invoice-name').value);
		invoiceArray.push(document.getElementById('invoice-last-name').value);
		invoiceArray.push(document.getElementById('invoice-country').value);
		invoiceArray.push(document.getElementById('invoice-province').value);
		invoiceArray.push(document.getElementById('invoice-city').value);
		invoiceArray.push(document.getElementById('invoice-cap').value);
		invoiceArray.push(document.getElementById('invoice-text').value);
		for (var i = 0; i < invoiceArray.length; i++) {
			if (invoiceArray[i] == "") {
				isInvoiceValid = false;
			}
		}
		var invoiceText = invoiceArray.join('-/-');
		var shippingArray = [];
		shippingArray.push(document.getElementById('shipping-name').value);
		shippingArray.push(document.getElementById('shipping-last-name').value);
		shippingArray.push(document.getElementById('shipping-country').value);
		shippingArray.push(document.getElementById('shipping-province').value);
		shippingArray.push(document.getElementById('shipping-city').value);
		shippingArray.push(document.getElementById('shipping-cap').value);
		shippingArray.push(document.getElementById('shipping-text').value);
		for (var i = 0; i < shippingArray.length; i++) {
			if (shippingArray[i] == "") {
				isShippingValid = false;
			}
		}
		var shippingText = shippingArray.join('-/-');
		if (isInvoiceValid) {
			setCookie('invoice-data',invoiceText,60);
			resultText += '<h2>Indirizzo di fatturazione aggiornato con successo</h2><br>';
		} else {
			resultText += '<h2 class="error">Indirizzo di fatturazione non valido</h2><br>';
		}
		if (isShippingValid) {
			setCookie('shipping-data',shippingText,60);
			resultText += '<h2>Indirizzo di spedizione aggiornato con successo</h2>';
		} else {
			resultText += '<h2 class="error">Indirizzo di spedizione non valido</h2>';
		}
		
		document.getElementById('result').innerHTML = resultText;
		// Load page if it's setted
		if (isShippingValid && isInvoiceValid) {
			var pageUrl = getParser('page');
			if (pageUrl != '') {
				window.location.href = pageUrl;
			}
		}
		
	}
	return false;
}

function getInvoiceData() {
	var cookie = readCookie('invoice-data');
	if (cookie == '') {return undefined;}
	return cookie.split('-/-');
}

function getShippingData() {
	var cookie = readCookie('shipping-data');
	if (cookie == '') {return undefined;}
	return cookie.split('-/-');
}

function resetUserData() {
	deleteCookie('shipping-data');
	deleteCookie('invoice-data');
	notUpdateData = true;
	window.location.reload();
	document.getElementById('result').innerHTML = '<h2>Dati resettati con successo</h2>';
}

function createInvoice() {
	// Generating text
	var invoiceText = '';
	var invoiceCookie = getInvoiceData();
	var shippingCookie = getShippingData();
	var totalPrice = 0.00;
	var totalNumber = 0;
	for (var i = 0; i < PRODUCTS.length; i++) {
		var cookie = readCookie('product-' + i);
		if (parseInt(cookie) > 0) {
			console.log('Writing element ' + i);
			var productPrice = PRODUCTS[i].price * parseInt(cookie);
			totalPrice += productPrice;
			totalNumber += parseInt(cookie);
			invoiceText += PRODUCTS[i].id + ' ' + PRODUCTS[i].name + ' \t' + cookie + '/' + PRODUCTS[i].n_available + ' \t' + productPrice.toFixed(2) + '\n';
		}
	}

	invoiceText += 'TOTALE\t\t' + totalNumber + ' \t' + totalPrice.toFixed(2) + '\n';
	invoiceText += '\nIndirizzo di fatturazione:\n'
	invoiceText += invoiceCookie[0] + ' ' + invoiceCookie[1] + '\n';
	invoiceText += invoiceCookie[2] + ' ' + invoiceCookie[4] + ' (' + invoiceCookie[3] + ') ' + invoiceCookie[5] + '\n';
	invoiceText += invoiceCookie[6] + '\n';
	invoiceText += '\nIndirizzo di spedizione:\n'
	invoiceText += shippingCookie[0] + ' ' + shippingCookie[1] + '\n';
	invoiceText += shippingCookie[2] + ' ' + shippingCookie[4] + ' (' + shippingCookie[3] + ') ' + shippingCookie[5] + '\n';
	invoiceText += shippingCookie[6];

	// Create and trigger download
	var time = new Date();
	var filename = time.toLocaleDateString() + ' - ' + time.toLocaleTimeString() + '.txt';
	download(filename, invoiceText);
	resetCart();
	window.location.href = 'index.html';
}