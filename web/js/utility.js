function setCookie(name,value,length)
{
  var expiry = new Date();
  var now = new Date();
  expiry.setTime(now.getTime() + (parseInt(length) * 60000));
  var textCookie = name + '=' + escape(value) + '; expires=' + expiry.toGMTString() + '; path=/';
  document.cookie = textCookie;
  console.log('Setted cookie: ' + textCookie);
}

function readCookie(name)
{
  if (document.cookie.length > 0)
  {
    var start = document.cookie.indexOf(name + "=");
    if (start != -1)
    {
      start = start + name.length + 1;
      var end = document.cookie.indexOf(";",start);
      if (end == -1) end = document.cookie.length;
      return unescape(document.cookie.substring(start,end));
    }else{
       return "";
    }
  }
  return "";
}

function deleteCookie(name)
{
  setCookie(name,'',-1);
}

// GET paramenters parser
function getParser(val) {
    var result = "",
        tmp = [];
    location.search
    //.replace ( "?", "" ) 
    // this is better, there might be a question mark inside
    .substr(1)
        .split("&")
        .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
    });
    return result;
}

function download(filename, text) {
    var wrapLink = document.createElement('a');
    wrapLink.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    wrapLink.setAttribute('download', filename);
    wrapLink.setAttribute('target', '_blank');

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        wrapLink.dispatchEvent(event);
    }
    else {
        wrapLink.click();
    }
}