function Product(id,name,desc,price,n_available,category,image,offert) {
	this.id = id;
	this.name = name;
	this.desc = desc;
	this.price = price.toFixed(2);
	this.n_available = n_available;
	this.n_selected = 0;
	this.category = category;
	this.image = image;
	this.offert = offert;

	this.getImage = function() {
		return "img/products/" + image;
	}

	this.parseHtml = function() {
		var html="";
		html += "<li>";
		html += "	<div data-id=\"" + this.id + "\" class=\"product-wrapper product-" + this.id + "\">";
		html += "		<img src=\""+ this.getImage() + "\" alt=\"" + this.name + "\" class=\"product-image\">";
		html += "		<div class=\"product-name\">" + this.name + "<\/div>";
		html += "		<div class=\"product-description\">" + this.desc + "<\/div>";
		html += "		<div class=\"product-qnt-wrapper\">";
		html += "			<table>";
		html += "				<tr>";
		html += "					<td><a href=\"javascript:void(0)\" onclick=\"removeElement(" + this.id + ")\"><i class=\"material-icons\">remove_circle<\/i><\/a><\/td>";
		html += "					<td class=\"product-qnt\">" + this.n_selected + "\/" + this.n_available + "<\/td>";
		html += "					<td><a href=\"javascript:void(0)\" onclick=\"addElement(" + this.id + ")\"><i class=\"material-icons\">add_circle<\/i><\/a><\/td>";
		html += "				<\/tr>";
		html += "			<\/table>";
		html += "		<\/div>";
		html += "		<div class=\"product-price-wrapper\">" + this.price + " €<\/div>";
		html += "	<\/div>";
		html += "<\/li>";

		return html;
	}

	this.parseInnerHtml = function() {
		var html="";
		html += "	<img src=\""+ this.getImage() + "\" alt=\"" + this.name + "\" class=\"product-image\">";
		html += "	<div class=\"product-name\">" + this.name + "<\/div>";
		html += "	<div class=\"product-description\">" + this.desc + "<\/div>";
		html += "	<div class=\"product-qnt-wrapper\">";
		html += "		<table>";
		html += "			<tr>";
		html += "				<td><a href=\"javascript:void(0)\" onclick=\"removeElement(" + this.id + ")\"><i class=\"material-icons\">remove_circle<\/i><\/a><\/td>";
		html += "				<td class=\"product-qnt\">" + this.n_selected + "\/" + this.n_available + "<\/td>";
		html += "				<td><a href=\"javascript:void(0)\" onclick=\"addElement(" + this.id + ")\"><i class=\"material-icons\">add_circle<\/i><\/a><\/td>";
		html += "			<\/tr>";
		html += "		<\/table>";
		html += "	<\/div>";
		html += "	<div class=\"product-price-wrapper\">" + this.price + " €<\/div>";

		return html;
	}

	this.addSelected = function(n) {
		var newValue = parseInt(this.n_selected) + parseInt(n);
		if ((newValue <= this.n_available) && (newValue >= 0)) {
			this.n_selected = newValue;
			this.update();
		}
	}

	this.update = function() {
		var elements = [];
		elements = document.getElementsByClassName('product-' + this.id);

		for (var i = 0; i < elements.length; i++) {
			elements[i].innerHTML = this.parseInnerHtml();
		};

		setCookie('product-' + this.id, this.n_selected, 60);
	}

	this.initialize = function() {
		var memVal = readCookie('product-' + this.id);
		if (memVal > 0) {
			this.n_selected = memVal;
			console.log('Product ' + this.id + ': n_selected setted to ' + this.n);
		}
	}
}

function addElement(id) {
	console.log('product ' + id + ': +1');
	for (var i = 0; i < PRODUCTS.length; i++) {
		if(PRODUCTS[i].id == id) {
			PRODUCTS[i].addSelected(1);
		}
	};
	updateCart()
	return false;
}

function removeElement(id) {
	console.log('product ' + id + ': -1');
	for (var i = 0; i < PRODUCTS.length; i++) {
		if(PRODUCTS[i].id == id) {
			PRODUCTS[i].addSelected(-1);
		}
	};
	updateCart()
	return false;
}

function Category(id,name) {
	this.id = id;
	this.name = name;
}

function updateCart() {
	var numProduct = 0;
	for (var i = 0; i < PRODUCTS.length; i++) {
		numProduct += parseInt(PRODUCTS[i].n_selected);
	};
	document.getElementById('cart').innerHTML = 'CARRELLO (' + numProduct + ')';
}

function resetCart() {
	for (var i = 0; i < PRODUCTS.length; i++) {
		PRODUCTS[i].addSelected(-PRODUCTS[i].n_selected);
	};
	updateCart();
}

// Categories must be an array (DEPRECATED)
/*
function productsFromCategories(searchCategories) {
	var lengthCategories = searchCategories.length;
	var lengthProducts = PRODUCTS.length;
	var validProd = [];

	for (var i = 0; i < lengthProducts; i++) {
		for (var k = 0; k < lengthCategories.length; k++) {
			if(PRODUCTS[i].id == searchCategories[k]) {
				validProd.push(PRODUCTS[i]);
			}
		};
	}
	return validProd;
} */

/* DEPRECATED
function productsFormOffert() {
	var lengthProducts = PRODUCTS.length;
	var validProd = [];

	for (var i = 0; i < lengthProducts; i++) {
		if(PRODUCTS[i].offert == true) {
			validProd.push(PRODUCTS[i]);
		};
	}
	return validProd;
} */

CATEGORIES = [
	new Category(0,'Frutta'),
	new Category(1,'Tuberi'),
	new Category(2,'Legumi'),
	new Category(3,'Verdura')
];
PRODUCTS = [
	new Product(0,'Mele','Mele rosse di prima scelta 100% italiane (1 Kg)',2.49,28,0,'mele.png',0),
	new Product(1,'Pere','Pere Abate di prima scelta 100% italiane (1 Kg)',2.19,40,0,'pere.png',1),
	new Product(2,'More','Cestino di more nere di stagione provenienti dai boschi italiani (250 Gr)',3.99,30,1,'more.png',0),
	new Product(3,'Fragole','Cestino di fragole di stagione (250 Gr)',0.99,30,0,'fragole.png',1),
	new Product(4,'Fagioli','Fagioli borlotti (1 Kg)',2.50,30,2,'fagioli.png',1),
	new Product(5,'Patate','Patate americane (1 Kg)',1.59,23,1,'patate.png',0),
	new Product(6,'Cavoli','Cavoli 100% italiani (1 Kg)',1.99,8,3,'cavoli.png',0),
	new Product(7,'Piselli','Piselli fini surgelati (1 Kg)',2.99,32,3,'piselli.png',1)
];

// Initialize products
for (var i = 0; i < PRODUCTS.length; i++) {
	PRODUCTS[i].initialize();
};